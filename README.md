# Docker Debian Buster - RVM - Rails - Octobox

This repository is used for buildingaq custom Docker image for the [Octobox](https://github.com/octobox/octobox) app.

## Name of This Docker Image
[registry.gitlab.com/jhsu802701/docker-debian-buster-rvm-rails-octobox](https://gitlab.com/jhsu802701/docker-debian-buster-rvm-rails-octobox/container_registry)

## Upstream Docker Image
[registry.gitlab.com/rubyonracetracks/docker-debian-buster-min-rvm](https://gitlab.com/rubyonracetracks/docker-debian-buster-min-rvm/container_registry)

## What's Added
* The latest version of Ruby
* The latest versions of the rails, pg, nokogiri, and ffi gems
* The versions of the above gems and Ruby version used in the Octobox app
* Bundler
* The mailcatcher gem

## Things NOT Included
This Docker image does not include all versions of Ruby, Rails, pg, nokogiri, and ffi.  Instead, I have custom Docker images for every Rails app I'm working on.

## What's the Point?
This Docker image is used for running the Rails Tutorial Sample App and for creating new apps.  The process of getting started is MUCH faster in a development environment that comes with the correct versions of Ruby and the correct versions of certain gems already pre-installed.  The rails, pg, nokogiri, and ffi gems take a long time to install.

## More Information
General information common to all Docker Debian build repositories is in the [FAQ](https://gitlab.com/rubyonracetracks/docker-debian-common/blob/master/FAQ.md).
